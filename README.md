# VPC

## Usage

```
module "vpc" {
  source = "github.com/terraform-community-modules/tf_aws_vpc?ref=v1.0.0"

  aws_access_key  = "${var.aws_access_key}"
  aws_secret_key  = "${var.aws_secret_key}"
}
```

## TODO

- the module should be able to create the vpc, with internal structure if it doesn't exists, but don't create if it already exists.

- the module should be able to contain a full product vpc with several application and domain services (resource groups), or just one application and/or domain service for dynamic/on-demand environments.