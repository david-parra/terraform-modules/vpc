terraform {
  backend "s3" {
    bucket = "iag-terraform-modules-vpc"
    region = "us-west-1"
  }
}